package utils

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"time"
)

type FileUtils interface {
	Read(name string)
	Write(name string, data interface{})
}

func GetCurrentTime() *time.Time {
	now := time.Now()
	return &now
}

func ReadFile(path string, d interface{}) error {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	err = json.Unmarshal(file, &d)
	if err != nil {
		return err
	}
	return nil
}

func WriteFile(path string, d interface{}) error {
	file, _ := json.MarshalIndent(d, "", "")
	err := ioutil.WriteFile(path, file, 0644)
	if err != nil {
		return err
	}
	return nil
}

func CreateFile(nameFile string) (*os.File, error) {
	f, err := os.Create(nameFile)
	if err != nil {
		return nil, err
	}
	return f, nil
}
