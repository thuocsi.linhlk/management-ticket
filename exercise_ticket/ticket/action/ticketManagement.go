package action

import (
	"fmt"
	"study/ticket/model"
	"study/ticket/utils.go"
)

func CrateTicket(ticket *model.TicketModel, file string) (*model.TicketModel, error) {
	var data []model.TicketModel
	err := utils.ReadFile(file, &data)
	if err == nil {
		utils.CreateFile(file)
	}
	mapData := append(data, *ticket)
	err = utils.WriteFile(file, mapData)
	if err != nil {
		return nil, err
	}
	return ticket, err
}

func UpdateTicket(ticket *model.TicketModel, file string) (*model.TicketModel, error) {
	var data []model.TicketModel
	err := utils.ReadFile(file, &data)
	if err != nil {
		return nil, err
	}
	for idx, d := range data {
		if d.Entity.ID == ticket.Entity.ID {
			data[idx] = *ticket
		}
	}
	err = utils.WriteFile(file, data)
	if err != nil {
		return nil, err
	}
	return ticket, nil
}

func DeleteTicket(ticket *model.TicketModel, file string) (*model.TicketModel, error) {
	var data []model.TicketModel
	err := utils.ReadFile(file, &data)
	if err != nil {
		return nil, err
	}
	//
	for idx, t := range data {
		if t.Entity.ID == ticket.Entity.ID {
			data[idx].Entity.IsEnabled = false
		}
	}
	fmt.Println(data)
	err = utils.WriteFile(file, data)
	if err != nil {
		return nil, err
	}
	return ticket, nil
}
