package action

import (
	"study/ticket/model"
	"study/ticket/utils.go"
)

func CrateTicketLog(ticketLog *model.TicketLogModel, file string) (*model.TicketLogModel, error) {
	var data []model.TicketLogModel
	err := utils.ReadFile(file, &data)
	if err == nil {
		utils.CreateFile(file)
	}

	mapData := append(data, *ticketLog)
	err = utils.WriteFile(file, mapData)
	if err != nil {
		return nil, err
	}
	return ticketLog, err
}

func CheckTicketLog(fileTicketLog string) int {
	var Id int = 1
	var ticketLogs []model.TicketLogModel
	err := utils.ReadFile(fileTicketLog, &ticketLogs)
	if err != nil {
		utils.CreateFile(fileTicketLog)
	} else {
		for idx, t := range ticketLogs {
			if t.Entity.ID != 0 {
				Id = ticketLogs[idx].Entity.ID + 1
			}
		}
	}
	return Id
}
