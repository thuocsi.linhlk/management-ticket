package action

import (
	"fmt"
	"study/ticket/model"
	"study/ticket/model/entity"
	"study/ticket/model/enum"
	"study/ticket/utils.go"
	"time"
)

func AssingSequentiallyStaff(fileTicket string, fileStaff string) ([]model.TicketModel, error) {
	var tickets []model.TicketModel
	err := utils.ReadFile(fileTicket, &tickets)
	if err != nil {
		return nil, err
	}

	var staffs []model.StaffModel
	err = utils.ReadFile(fileStaff, &staffs)
	if err != nil {
		return nil, err
	}

	for idt, ticket := range tickets {
		if ticket.Status == string(enum.TicketStatus.NEW) {
			tickets[idt].Status = string(enum.TicketStatus.ASSIGN)
			for ids, staff := range staffs {
				if staff.StatusStaff == string(enum.StaffStatus.NOTASSIGN) {
					tickets[idt].StaffId = staff.Entity.ID
				}
				staffs[ids].StatusStaff = string(enum.StaffStatus.ASSIGN)
				break
			}
		}
	}

	err = utils.WriteFile(fileTicket, tickets)
	if err != nil {
		return nil, err
	} else {
		for _, ticket := range tickets {
			ticketLog := &model.TicketLogModel{
				Entity: entity.EntityModel{
					ID:           CheckTicketLog("ticketLog.json"),
					IsEnabled:    true,
					CreateWhen:   time.Now(),
					CreateBy:     1,
					ModifiedWhen: time.Now(),
					ModifiedBy:   1,
					Code:         ticket.Entity.Code,
					Name:         ticket.Entity.Name,
				},
				Status:   ticket.Status,
				StaffId:  ticket.StaffId,
				TicketId: ticket.Entity.ID,
			}
			CrateTicketLog(ticketLog, "ticketLog.json")
		}
	}

	err = utils.WriteFile(fileStaff, staffs)
	if err != nil {
		return nil, err
	}

	return tickets, nil

}

func AssingManyStaff(fileTicket string, fileStaff string) ([]model.TicketModel, error) {
	var tickets []model.TicketModel
	err := utils.ReadFile(fileTicket, &tickets)
	if err != nil {
		return nil, err
	}

	var staffs []model.StaffModel
	err = utils.ReadFile(fileStaff, &staffs)
	if err != nil {
		return nil, err
	}

	for idt, ticket := range tickets {
		if ticket.Status == string(enum.TicketStatus.NEW) {
			tickets[idt].Status = string(enum.TicketStatus.ASSIGN)
			for ids, staff := range staffs {
				if staff.StatusStaff == string(enum.StaffStatus.NOTASSIGN) {
					tickets[idt].StaffId = 0
					tickets[idt].ListStaffId = append(tickets[idt].ListStaffId, staff.Entity.ID)
					fmt.Println("tickets[idt].ListStaffId")
					fmt.Println(tickets[idt].ListStaffId)
				}
				staffs[ids].StatusStaff = string(enum.StaffStatus.ASSIGN)
			}
		}
	}

	err = utils.WriteFile(fileTicket, tickets)
	if err != nil {
		return nil, err
	} else {
		for _, ticket := range tickets {
			ticketLog := &model.TicketLogModel{
				Entity: entity.EntityModel{
					ID:           CheckTicketLog("ticketLog.json"),
					IsEnabled:    true,
					CreateWhen:   time.Now(),
					CreateBy:     1,
					ModifiedWhen: time.Now(),
					ModifiedBy:   1,
					Code:         ticket.Entity.Code,
					Name:         ticket.Entity.Name,
				},
				Status:      ticket.Status,
				StaffId:     ticket.StaffId,
				TicketId:    ticket.Entity.ID,
				ListStaffId: ticket.ListStaffId,
			}
			CrateTicketLog(ticketLog, "ticketLog.json")
		}
	}

	err = utils.WriteFile(fileStaff, staffs)
	if err != nil {
		return nil, err
	}

	return tickets, nil

}
