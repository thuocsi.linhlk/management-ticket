package action

import (
	"fmt"
	"study/ticket/model"
	"study/ticket/utils.go"
)

func CrateStaff(staff *model.StaffModel, file string) (*model.StaffModel, error) {
	var data []model.StaffModel
	err := utils.ReadFile(file, &data)
	if err == nil {
		utils.CreateFile(file)
	}

	mapData := append(data, *staff)
	err = utils.WriteFile(file, mapData)
	if err != nil {
		return nil, err
	}
	return staff, err
}

func UpdateStaff(staff *model.StaffModel, file string) (*model.StaffModel, error) {
	var data []model.StaffModel
	err := utils.ReadFile(file, &data)
	if err != nil {
		return nil, err
	}
	for idx, d := range data {
		if d.Entity.ID == staff.Entity.ID {
			data[idx] = *staff
		}
	}
	err = utils.WriteFile(file, data)
	if err != nil {
		return nil, err
	}
	return staff, nil
}

func DeleteStaff(staffId int, file string) error {
	var data []model.StaffModel
	err := utils.ReadFile(file, &data)
	if err != nil {
		return err
	}
	//
	for idx, t := range data {
		if t.Entity.ID == staffId {
			data[idx].Entity.IsEnabled = false
		}
	}
	fmt.Println(data)
	err = utils.WriteFile(file, data)
	if err != nil {
		return err
	}
	return nil
}
