package model

import (
	"study/ticket/model/entity"
)

type StaffModel struct {
	Entity      entity.EntityModel `json:"entity,omitempty"  bson:"entity,omitempty"`
	StatusStaff string             `json:"StatusStaff,omitempty"  bson:"StatusStaff,omitempty"`
	LastName    string             `json:"lastName,omitempty"  bson:"LastName,omitempty"`
	FirstName   string             `json:"firstName,omitempty"  bson:"firstName,omitempty"`
	Mail        string             `json:"mail,omitempty"  bson:"mail,omitempty"`
}
