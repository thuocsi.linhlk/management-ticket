package model

import (
	"study/ticket/model/entity"
	"time"
)

type TicketModel struct {
	Entity entity.EntityModel `json:"entity,omitempty"  bson:"entity,omitempty"`

	TicketLogId  int       `json:"ticketLogId,omitempty"  bson:"ticketLogId,omitempty"`
	Status       string    `json:"status,omitempty"  bson:"status,omitempty"`
	StaffId      int       `json:"staffId,omitempty"  bson:"staffId,omitempty"`
	ListStaffId  []int     `json:"listStaffId,omitempty"  bson:"listStaffId,omitempty"`
	DeadlineTime time.Time `json:"deadlineTime,omitempty"  bson:"deadlineTime,omitempty"`
	Description  string    `json:"description,omitempty"  bson:"description,omitempty"`
}
