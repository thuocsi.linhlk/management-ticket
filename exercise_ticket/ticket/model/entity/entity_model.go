package entity

import "time"

type EntityModel struct {
	ID           int       `json:"id,omitempty"  bson:"id,omitempty"`
	IsEnabled    bool      `json:"isEnabled,omitempty"  bson:"isEnabled,omitempty"`
	CreateWhen   time.Time `json:"createWhen,omitempty"  bson:"createWhen,omitempty"`
	CreateBy     int       `json:"createBy,omitempty"  bson:"createBy,omitempty"`
	ModifiedWhen time.Time `json:"modifiedWhen,omitempty"  bson:"modifiedWhen,omitempty"`
	ModifiedBy   int       `json:"modifiedBy,omitempty"  bson:"modifiedBy,omitempty"`
	Code         string    `json:"code,omitempty"  bson:"code,omitempty"`
	Name         string    `json:"name,omitempty"  bson:"name,omitempty"`
}
