package enum

type TicketStatusValue string
type TicketStatusEnum struct {
	NEW      TicketStatusValue
	ASSIGN   TicketStatusValue
	CANCELED TicketStatusValue
	DONE     TicketStatusValue
}

var TicketStatus = &TicketStatusEnum{
	NEW:      "NEW",
	ASSIGN:   "ASSIGN",
	CANCELED: "CANCELED",
	DONE:     "DONE",
}

type StaffStatusEnum struct {
	ASSIGN    TicketStatusValue
	NOTASSIGN TicketStatusValue
}

var StaffStatus = &StaffStatusEnum{
	ASSIGN:    "ASSIGN",
	NOTASSIGN: "NEW",
}
