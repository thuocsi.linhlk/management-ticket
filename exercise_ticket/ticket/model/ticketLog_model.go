package model

import (
	"study/ticket/model/entity"
)

type TicketLogModel struct {
	Entity      entity.EntityModel `json:"entity,omitempty"  bson:"entity,omitempty"`
	Status      string             `json:"status,omitempty"  bson:"status,omitempty"`
	StaffId     int                `json:"staffId,omitempty"  bson:"staffId,omitempty"`
	TicketId    int                `json:"ticketId,omitempty"  bson:"ticketId,omitempty"`
	ListStaffId []int              `json:"listStaffId,omitempty"  bson:"listStaffId,omitempty"`
}
