package main

import (
	"study/ticket/action"
	"study/ticket/model"
	"study/ticket/model/entity"
	"study/ticket/model/enum"
	"time"
)

type mytype []model.TicketModel

var fileStaff = "staff.json"
var fileTicket = "ticket.json"
var fileTicletLog = "ticketLog.json"

func Staff() {
	staff := &model.StaffModel{
		Entity: entity.EntityModel{
			ID:           1,
			IsEnabled:    true,
			CreateWhen:   time.Now(),
			CreateBy:     1,
			ModifiedWhen: time.Now(),
			ModifiedBy:   1,
			Code:         "dsctest1",
			Name:         "dsctest1",
		},
		StatusStaff: string(enum.StaffStatus.NOTASSIGN),
		LastName:    "dsctest1",
		FirstName:   "dsctest1",
		Mail:        "dsctest1",
	}
	action.CrateStaff(staff, fileStaff)
	// action.UpdateStaff(staff, fileStaff)
	// action.DeleteStaff(1, fileStaff)
}

func Ticket() {
	ticket := &model.TicketModel{
		Entity: entity.EntityModel{
			ID:           1,
			IsEnabled:    true,
			CreateWhen:   time.Now(),
			CreateBy:     1,
			ModifiedWhen: time.Now(),
			ModifiedBy:   1,
			Code:         "test",
			Name:         "test",
		},
		TicketLogId:  1,
		Status:       string(enum.TicketStatus.NEW),
		DeadlineTime: time.Now(),
		Description:  "test",
	}
	createData, createErr := action.CrateTicket(ticket, fileTicket)
	if createErr == nil {
		ticketLog := &model.TicketLogModel{
			Entity: entity.EntityModel{
				ID:           action.CheckTicketLog(fileTicletLog),
				IsEnabled:    true,
				CreateWhen:   time.Now(),
				CreateBy:     1,
				ModifiedWhen: time.Now(),
				ModifiedBy:   1,
				Code:         createData.Entity.Code,
				Name:         createData.Entity.Name,
			},
			Status:   createData.Status,
			StaffId:  createData.StaffId,
			TicketId: createData.Entity.ID,
		}
		action.CrateTicketLog(ticketLog, fileTicletLog)
	}

	updateData, updateErr := action.UpdateTicket(ticket, fileTicket)
	if updateErr == nil {
		ticketLog := &model.TicketLogModel{
			Entity: entity.EntityModel{
				ID:           action.CheckTicketLog(fileTicletLog),
				IsEnabled:    true,
				CreateWhen:   time.Now(),
				CreateBy:     1,
				ModifiedWhen: time.Now(),
				ModifiedBy:   1,
				Code:         updateData.Entity.Code,
				Name:         updateData.Entity.Name,
			},
			Status:   updateData.Status,
			StaffId:  updateData.StaffId,
			TicketId: updateData.Entity.ID,
		}
		action.CrateTicketLog(ticketLog, fileTicletLog)
	}

	deleteData, err := action.DeleteTicket(ticket, fileTicket)
	if err == nil {
		ticketLog := &model.TicketLogModel{
			Entity: entity.EntityModel{
				ID:           action.CheckTicketLog(fileTicletLog),
				IsEnabled:    true,
				CreateWhen:   time.Now(),
				CreateBy:     1,
				ModifiedWhen: time.Now(),
				ModifiedBy:   1,
				Code:         deleteData.Entity.Code,
				Name:         deleteData.Entity.Name,
			},
			Status:   deleteData.Status,
			StaffId:  deleteData.StaffId,
			TicketId: deleteData.Entity.ID,
		}
		action.CrateTicketLog(ticketLog, fileTicletLog)
	}
}

func main() {
	Staff()
	// data, err := action.AssingSequentiallyStaff(fileTicket, fileStaff)
	// if err == nil {
	// 	fmt.Println(data)
	// }

	// data, err := action.AssingManyStaff(fileTicket, fileStaff)
	// if err == nil {
	// 	fmt.Println(data)
	// }
}
